const path = require('path');

module.exports = {
    entry: './receiveServer1.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist')
    },
    target: 'node',
    optimization: {
        minimize: true
    },
    devServer: {
        port: 9000,
        compress: true,
        open: true
    }
};
