const maps = {
    replaceFrom: [
        /^(й)/,
        /^(Й)/,
        /(ай)/g,
        /(ей)/g,
        /(ий)/g,
        /(ой)/g,
        /(уй)/g,
        /(ый)/g,
        /(эй)/g,
        /(юй)/g,
        /(яй)/g,
        /(АЙ)/g,
        /(ЕЙ)/g,
        /(ИЙ)/g,
        /(ОЙ)/g,
        /(УЙ)/g,
        /(ЫЙ)/g,
        /(ЭЙ)/g,
        /(ЮЙ)/g,
        /(ЯЙ)/g
    ],
    replaceTo: [
        'y',
        'Y',
        'ay',
        'ey',
        'iy',
        'oy',
        'uy',
        'yi',
        'ey',
        'yuy',
        'yay',
        'AY',
        'EY',
        'IY',
        'OY',
        'UY',
        'YI',
        'EY',
        'YUY',
        'YAY'
    ],
    translitCharsSource: {
        а: 'a',
        б: 'b',
        в: 'v',
        г: 'g',
        д: 'd',
        е: 'e',
        ё: 'e',
        ж: 'zh',
        з: 'z',
        и: 'i',
        й: 'i',
        к: 'k',
        л: 'l',
        м: 'm',
        н: 'n',
        о: 'o',
        п: 'p',
        р: 'r',
        с: 's',
        т: 't',
        у: 'u',
        ф: 'f',
        х: 'kh',
        ц: 'ts',
        ч: 'ch',
        ш: 'sh',
        щ: 'sch',
        ъ: '',
        ы: 'y',
        ь: '',
        э: 'e',
        ю: 'yu',
        я: 'ya',
        А: 'A',
        Б: 'B',
        В: 'V',
        Г: 'G',
        Д: 'D',
        Е: 'E',
        Ё: 'E',
        Ж: 'Zh',
        З: 'Z',
        И: 'I',
        Й: 'I',
        К: 'K',
        Л: 'L',
        М: 'M',
        Н: 'N',
        О: 'O',
        П: 'P',
        Р: 'R',
        С: 'S',
        Т: 'T',
        У: 'U',
        Ф: 'F',
        Х: 'Kh',
        Ц: 'Ts',
        Ч: 'Ch',
        Ш: 'Sh',
        Щ: 'Sch',
        Ъ: '',
        Ы: 'Y',
        Ь: '',
        Э: 'E',
        Ю: 'Yu',
        Я: 'Ya'
    }
};

function transliterateWord(target) {
    let i,
        l,
        char,
        translitChar,
        result = '',
        wordLength,
        word = `${target}`;

    for (i = 0, l = maps.replaceFrom.length; i < l; i++) {
        word = word.replace(maps.replaceFrom[i], maps.replaceTo[i]);
    }

    wordLength = word.length;

    for (i = 0; i < wordLength; i++) {
        char = word.charAt(i);
        translitChar = maps.translitCharsSource[char];
        result += typeof translitChar !== 'undefined' ? translitChar : char;
    }

    if (word.toUpperCase() === word) {
        result = result.toUpperCase();
    }

    return result;
}

module.exports = transliterateWord;
