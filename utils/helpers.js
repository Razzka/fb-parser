const transliterate = require('./transliterate.js');

const getIp = req => {
    let ip = '';

    try {
        ip = req.headers['x-forwarded-for']
        || req.connection.remoteAddress
        || req.socket.remoteAddress
        || req.connection.socket.remoteAddress;
    } catch (ipErr) {
        console.log('Error while getting IP: ', ipErr);
    }

    return ip;
};

function getDataFromQuery(req, response) {
    let data;

    const ip = getIp(req);

    try {
        data = JSON.parse(req.query.data);
        console.log('Received data', data, ip);
    } catch (e) {
        console.log('Error parsing data', req.query.data, ip);
        response.statusCode = 400;
        response.send(e);
        return undefined;
    }

    return data;
}

function getEmailKey(email) {
    return email.toLowerCase();
}

function getFioKey({ f, i, fi }) {
    if (f && i) {
        return `${f}_${i}`.toLowerCase();
    }

    if (fi) {
        return fi.replace(' ', '_').toLowerCase();
    }

    return null;
}

function isCyrrilic(text) {
    return /[а-яА-ЯЁё]/.test(text);
}

function getFio(fio) {
    let fi;
    let fiEng;

    if (isCyrrilic(fio)) {
        fi = fio;
        fiEng = transliterate(fio);
    } else {
        fi = transliterate(fio);
        fiEng = fio;
    }

    return {
        fi,
        fiEng
    };
}

function removeDuplicates(myArr, prop1, prop2) {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => `${mapObj[prop1]}_${mapObj[prop2]}`).indexOf(`${obj[prop1]}_${obj[prop2]}`) === pos;
    });
}

module.exports = {
    getDataFromQuery,
    getEmailKey,
    getFioKey,
    getFio,
    isCyrrilic,
    removeDuplicates
};
