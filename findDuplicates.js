const fs = require('fs');
const { read } = require('./receiveSheets.js');
const {
    getEmailKey,
    getFioKey,
    getFio
} = require('./utils/helpers.js');

const streamEmails = fs.createWriteStream('findEmails.txt', { flags: 'a' });
const streamFio = fs.createWriteStream('findFio.txt', { flags: 'a' });

const duplicatesFio = [];
const duplicatesEmail = [];

const INITIAL_CACHE = {
    sheet1: {
        fi: {},
        fiEng: {},
        email: {}
    },
    sheet2: {
        fi: {},
        fiEng: {},
        email: {}
    },
    sheet3: {
        fi: {},
        fiEng: {},
        email: {}
    }
};

const cache = INITIAL_CACHE;

function updateCacheItem(sheet, field, { key, value, row }) {
    if (key) {
        const cacheItem = cache[sheet][field][key];

        if (cacheItem) {
            if (field === 'fi') {
                if (duplicatesFio.indexOf(key) === -1) { duplicatesFio.push(key); }
            }

            if (field === 'email') {
                if (duplicatesEmail.indexOf(key) === -1) { duplicatesEmail.push(key); }
            }

            cacheItem.push({ user: value, row });
        } else {
            cache[sheet][field][key] = [{ user: value, row }];
        }
    }
}

function insertFio(row, idx, f, i) {
    const { fi, fiEng } = getFio(getFioKey({ f, i }));
    const value = {
        fio: `${row[1]} ${row[0]}`,
        city: row[2],
        age: row[3],
        position: row[4],
        email: row[5]
    };

    updateCacheItem('sheet3', 'fi', ({ key: fi, value, row: idx }));
    updateCacheItem('sheet3', 'fiEng', ({ key: fiEng, value, row: idx }));
}

function loadData() {
    read('Sheet3!A:K').then(data => {
        for (let i = 0; i < data.length; i++) {
            const row = data[i];

            if (row[0] && row[1]) {
                insertFio(row, i, row[0], row[1]);
                insertFio(row, i, row[1], row[0]);
            }

            if (row[5]) {
                const value = {
                    fio: `${row[0]} ${row[1]}`,
                    city: row[2],
                    age: row[3],
                    position: row[4],
                    email: row[5]
                };

                updateCacheItem('sheet3', 'email', ({ key: getEmailKey(row[5]), value, row: i }));
            }
        }

        for (let i = 0; i < duplicatesEmail.length; i++) {
            const key = duplicatesEmail[i];
            const value = cache.sheet3.email[key];

            streamEmails.write(`Rows: ${value.map(v => v.row).join(', ')}
${value.map(v => {
        const user = v.user;

        return `${user.fio} ${user.city} ${user.age} ${user.position} ${user.email}\n`;
    }).join('')}\n`);
        }

        for (let i = 0; i < duplicatesFio.length; i++) {
            const key = duplicatesFio[i];
            const value = cache.sheet3.fi[key];

            streamFio.write(`Rows: ${value.map(v => v.row).join(', ')}
${value.map(v => {
        const user = v.user;

        return `${user.fio} ${user.city} ${user.age} ${user.position} ${user.email}\n`;
    }).join('')}\n`);
        }
    }).catch(e => {
        console.log('Error loading data', e);
    });
}

loadData();
