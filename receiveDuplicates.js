const { read } = require('./receiveSheets.js');
const transliterate = require('./utils/transliterate.js');
const {
    getDataFromQuery,
    getEmailKey,
    getFioKey,
    getFio,
    removeDuplicates
} = require('./utils/helpers.js');

const FIVE_MINUTES = 1000 * 60 * 5;
const getInitialCache = () => ({
    sheet1: {
        fi: {},
        fiEng: {},
        email: {}
    },
    sheet2: {
        fi: {},
        fiEng: {},
        email: {}
    },
    sheet3: {
        fi: {},
        fiEng: {},
        email: {}
    }
});

let cache = getInitialCache();
let nicks = [];

function addUserToCache({
    f = '', i = '', city = '', age = '', email = '', position = ''
}, row) {
    const value = {
        fio: `${f} ${i}`,
        city
    };

    if (f && i) {
        updateCacheItem('sheet3', 'fi', {
            key: getFioKey({ f, i }),
            value,
            row
        });
    }

    if (email) {
        updateCacheItem('sheet3', 'email', {
            key: getEmailKey(email),
            value,
            row
        });
    }
}

function updateCacheItem(sheet, field, { key, value, row }) {
    if (key) {
        const cacheItem = cache[sheet][field][key];

        if (cacheItem) {
            cacheItem.push({ user: value, row });
        } else {
            cache[sheet][field][key] = [{ user: value, row }];
        }
    }
}

function updateCache(data, sheet) {
    for (let i = 0; i < data.length; i++) {
        const row = data[i];

        if (sheet === 3 && row[0] && row[1]) {
            const { fi, fiEng } = getFio(getFioKey({ f: row[0], i: row[1] }));
            const value = {
                fio: `${row[0]} ${row[1]}`,
                city: row[2],
                age: row[3],
                position: row[4],
                email: row[5]
            };

            updateCacheItem(`sheet${sheet}`, 'fi', ({ key: fi, value, row: i }));
            updateCacheItem(`sheet${sheet}`, 'fiEng', ({ key: fiEng, value, row: i }));
        }

        if (sheet < 3 && row[0]) {
            const { fi, fiEng } = getFio(getFioKey({ fi: row[0] }));
            const value = {
                fio: row[0],
                city: row[1],
                position: sheet === 1 ? row[2] : undefined
            };

            updateCacheItem(`sheet${sheet}`, 'fi', ({ key: fi, value, row: i }));
            updateCacheItem(`sheet${sheet}`, 'fiEng', ({ key: fiEng, value, row: i }));
        }

        if (row[5]) {
            const value = {
                fio: `${row[0]} ${row[1]}`,
                city: row[2],
                age: row[3],
                position: row[4],
                email: row[5]
            };

            updateCacheItem(`sheet${sheet}`, 'email', ({ key: getEmailKey(row[5]), value, row: i }));
        }
    }
}

function loadData() {
    const quotaUser = 'receiveDuplicates';

    Promise.all([
        read('Sheet1!A:C', quotaUser),
        read('Sheet2!A:B', quotaUser),
        read('Sheet3!A:K', quotaUser)
    ]).then(results => {
        cache = getInitialCache();
        nicks = results[2].map((row, idx) => ({
            search: `${row[7] || ''}_${row[8] || ''}_${row[9] || ''}_${row[10] || ''}`.toLowerCase(),
            user: {
                fio: `${row[0]} ${row[1]}`,
                city: row[2],
                age: row[3],
                position: row[4],
                email: row[5]
            },
            sheet: 3,
            row: idx
        }));
        for (let sheet = 1; sheet <= results.length; sheet++) {
            updateCache(results[sheet - 1], sheet);
        }

        setTimeout(loadData, FIVE_MINUTES);
    }).catch(e => {
        console.log('Error loading data', e);
        setTimeout(loadData, FIVE_MINUTES);
    });
}

function checkFio({ f, i }) {
    let result = [];

    for (let sheet = 1; sheet <= 3; sheet++) {
        const fi = getFioKey({ f, i });
        const fiEng = transliterate(fi);

        result = result.concat((
            cache[`sheet${sheet}`].fi[fi] || []
        ).map(e => Object.assign({}, e, { sheet })));

        result = result.concat((
            cache[`sheet${sheet}`].fiEng[fiEng] || []
        ).map(e => Object.assign({}, e, { sheet })));
    }

    return result;
}

function duplicates(req, response) {
    console.log('-------------CHECK DUPLICATES--------------');
    const data = getDataFromQuery(req, response);

    if (!data) {
        return;
    }

    let result = [];

    if (data.fio) {
        let [f, i] = data.fio.split(' ');

        result = result.concat(checkFio({ f, i }));

        [i, f] = data.fio.split(' ');
        result = result.concat(checkFio({ f, i }));
    }

    if (data.email) {
        for (let sheet = 1; sheet <= 3; sheet++) {
            result = result.concat((
                cache[`sheet${sheet}`].email[getEmailKey(data.email)] || []
            ).map(e => Object.assign({}, e, { sheet })));
        }
    }

    if (data.nick) {
        result = result.concat(nicks.filter(nick => nick.search.indexOf(data.nick.toLowerCase()) !== -1));
    }

    const unique = removeDuplicates(result, 'sheet', 'row');

    response.statusCode = 200;
    response.send(JSON.stringify(unique));
}

loadData();

module.exports = {
    duplicates,
    addUserToCache
};
