const express = require('express');
const moment = require('moment');
const { read, update } = require('./receiveSheets.js');
const { getDataFromQuery } = require('./utils/helpers.js');
const { addUserToCache, duplicates } = require('./receiveDuplicates.js');
const app = express();

const BATCH_COUNT = 15000;

const findLastRow = (currentRow, quotaUser) => {
    return read(`Sheet3!A${currentRow}:U${currentRow}`, quotaUser).then(row => {
        if (!row) {
            return Promise.resolve(currentRow);
        }

        return read(`Sheet3!S${currentRow}:S${currentRow + BATCH_COUNT}`).then(res => {
            const resultsRow = res.length;

            return findLastRow(currentRow + resultsRow, quotaUser);
        });
    });
};

const getComment = ({
    comment,
    contacts
}) => {
    let result = comment || '';

    for (let i = 0; i < contacts.length; i++) {
        const { type, value } = contacts[i];

        if (!value) {
            continue;
        }

        result = (result === '' ? `${type}: ${value}` : `${result}
${type}: ${value}`);
    }

    return result;
};

// Очередь тасков на вставку в гуглдок.
const queue = [];

const nextTask = () => {
    queue.shift();

    console.log('Next task, length: ', queue.length);

    if (queue.length !== 0) {
        queue[0]();
    }
};

app.get('/test', function(req, response) {
    response.send('Test OK');
});

app.get('/duplicates', duplicates);

app.get('/', function(req, response) {
    const data = getDataFromQuery(req, response);

    if (!data) {
        return;
    }

    const doWork = () => read('Sheet3!S:S', data.recruiter).then(res => {
        const resultsRow = res.length;

        return findLastRow(resultsRow, data.recruiter).then(lastRow => {
            return update(`Sheet3!A${lastRow}:W${lastRow}`, [
                data.surname, // Фамилия
                data.name, // Имя
                data.city, // city
                data.age || data.birthDay || '', // age
                data.position, // position
                data.email, // емейл
                data.telegram || '', // Логин в telegram
                data.phone, // phone
                data.github, // github
                data.HH || '', // hh
                data.MK, // Мой Круг
                data.linked, // linked
                data.source, // Источник контакта
                moment().format('DD.MM.YYYY'), // Дата последнего контакта
                data.inviteType, // Тип приглашения
                '', // Ответ
                '', // Что ответил кандидат
                '', // Прохождение теста
                data.recruiter, // Имя рекрутера
                '', // Оценка
                getComment({
                    comment: data.comment,
                    contacts: [{
                        type: 'skype',
                        value: data.skype
                    }, {
                        type: 'FB',
                        value: data.FB
                    }]
                }), // Комментарий
                '', // Клёвый кандидат
                '' // Новая ссылка
            ]).then(() => {
                console.log('Success');

                addUserToCache({
                    f: data.surname,
                    i: data.name,
                    city: data.city,
                    age: data.age || data.birthDay || '',
                    email: data.email,
                    position: data.position
                }, lastRow);

                nextTask();

                response.send('OK');
            }).catch(err => {
                console.log('Error: ', err);

                nextTask();

                response.statusCode = 400;
                response.send(err);
            });
        }).catch(err => {
            console.log('Error: ', err);

            nextTask();

            response.statusCode = 400;
            response.send(err);
        });
    }).catch(err => {
        console.log('Error: ', err);

        nextTask();

        response.statusCode = 400;
        response.send(err);
    });

    queue.push(doWork);

    console.log('task queued, length: ', queue.length);

    if (queue.length === 1) {
        queue[0]();
    }
});

app.listen(80, function() {
    console.log('Receive server started');
});
