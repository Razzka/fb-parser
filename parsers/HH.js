// anna.ivanova@csssr.com
// wiKIvy94

// TOKEN TS0J8GORG3C36CK06MHA2TULUKAU2JU0VVD8OF77826CPHVJTVJIB7IRP1C5NM15 // expires 07.09.2018
// refresh IQKCIBK0ED26U52U3VNQ5QFLQQ11JVN7NUKU8C0FTNOP19PQDLNR3SGSS1OVMS57

const fs = require('fs');
const request = require('superagent');

const agent = request.agent();

/* eslint-enable */
const TOKEN = 'TS0J8GORG3C36CK06MHA2TULUKAU2JU0VVD8OF77826CPHVJTVJIB7IRP1C5NM15';

const parseId = id => {
    return new Promise((resolve, reject) => {
        agent.get(`https://api.hh.ru/resumes/${id}`)
            .withCredentials()
            .set('User-Agent', 'Mozilla/5.0')
            .set('Authorization', `Bearer ${TOKEN}`)
            .then(res => {
                fs.writeFile('LAST_HH.txt', res.text, err => {
                    if (err) {
                        console.error(err);
                    }
                    console.log('Html stored to', 'LAST_HH.txt');
                });

                // const $ = cheerio.load(res.text);
                // const { name, surname } = getFio($);
                // const age = getAge($);
                // const city = getCity($);

                // const { email, phone, comment } = getContacts($);

                // const result = {
                //     name,
                //     surname,
                //     age,
                //     city,
                //     phone,
                //     comment,
                //     email
                // };

                // resolve(result);
            })
            .catch(e => reject(e));
    });
};

module.exports = parseId;
