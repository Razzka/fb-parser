const cheerio = require('cheerio');
const fs = require('fs');
const request = require('superagent');

let authorized = false;

const agent = request.agent();

const authorize = () => {
    if (authorized) {
        return Promise.resolve();
    }

    return new Promise((resolve, reject) => {
        agent.post('https://moikrug.ru/users/sign_in')
            .withCredentials()
            .set('upgrade-insecure-requests', '1')
            .set('User-Agent', 'Mozilla/5.0')
            .set('content-type', 'application/x-www-form-urlencoded')
            .set('Cookie', 'check_cookies=true; mid=FjMT8KoOc3PnLiFcamFLuw; _ga=GA1.2.974630593.1531064608; _gid=GA1.2.1717862396.1531064608; _ym_uid=15310646081020009658; _ym_d=1531064608; _ym_isad=2; jv_enter_ts_baDiD33Ffd=1531064608542; jv_visits_count_baDiD33Ffd=1; jv_utm_baDiD33Ffd=; jv_pages_count_baDiD33Ffd=1; _moikrug_session=ZUNtS0c4bnl1RXdaZVdUVFlnV09BS3JHRmdEVjdwQTkzM2dNSTFuMm5rbDN6UlhJeHZVclh1OGJRRVcxWXp1ZmxYL0llK25yVGl6MVlwclNPaVVXelZwNFZDR0tLbUtnN0gxN3NPalFBWVBNNTl2Y21ObHMzK3dlOTFLa3ArVXVwdGk1MTNTT29ucisyYndBcG1mcE1nPT0tLXJoMUtjUUluampPNHpXRGZlWFZ3d0E9PQ%3D%3D--26345af1e6e00c4f9640bd5b06b4695f42576b41; _gat=1; _gali=new_user')
            .send({
                user: {
                    email: 'Razko90@mail.ru',
                    password: 'logan123'
                },
                authenticity_token: 'j5ODI/G+TQMBb2VywkzAxu3Y6/3/+/weG6oXrBLSwKUPmgwoT7KvA1JdjbzZ9hTYFMPwc3u1XAADiNW7KcFEoA=='
            }).then(res => {
                fs.writeFile('AUTH_MK.txt', res.text, err => {
                    if (err) {
                        console.error(err);
                    }
                    console.log('Html stored to', 'AUTH_MK.txt');
                });
                if (res.text.indexOf('<span class="username">Александр</span>') === -1) {
                    console.error('Проблемы с авторизацией MK!!!');
                } else {
                    authorized = true;
                }
                console.log(res.statusCode);
                Promise.resolve();
            }).catch(err => {
                console.log('MK Auth err', err);
                Promise.reject(err);
            });
    });
}

authorize();

const getFio = (text, userId) => {
    const fioSearchString = `<a href="/${userId}">`;
    const fioIdx = text.indexOf(fioSearchString);
    const fio = fioIdx !== -1 ?
        text.substring(fioIdx + fioSearchString.length, text.indexOf('</a>', fioIdx)) :
        'None';
    let name, surname;

    if (fio.indexOf(' ') !== -1) {
        [name, surname] = fio.split(' ');
    } else {
        name = fio;
    }

    return {
        name, surname
    };
};

const getAge = text => {
    const searchStr = '<div class="label">Возраст: </div>';
    const ageIdx = text.indexOf(searchStr);

    if (ageIdx !== -1) {
        const startIdx = text.indexOf('>', ageIdx + searchStr.length + 1) + 1;
        const endIdx = text.indexOf('</div>', startIdx);

        return text.substr(startIdx, endIdx - startIdx).split(' ')[0];
    }

    return '';
};

const getCity = text => {
    const searchStr = '<div class="geo">';
    const idx = text.indexOf(searchStr);

    if (idx !== -1) {
        const startIdx = idx + searchStr.length;
        const endIdx = text.indexOf('</div>', idx);

        return text.substr(startIdx, endIdx - startIdx);
    }

    return '';
};

const getContacts = text => {
    const $ = cheerio.load(text);

    const contacts = $('.contacts > .contact');
    let comment = '';
    let email = '';
    let phone = '';

    for (let i = 0; i < contacts.length; i++) {
        const contact = $(contacts[i]);
        const type = contact.find('.type').text().replace(': ', '');
        const value = contact.find('.value').text();

        if (type === 'Почта') {
            email = value;
        } else if (type === 'Телефон') {
            phone = value;
        } else {
            comment = (comment === '' ? `${type}: ${value}` : `${comment}
${type}: ${value}`);
        }
    }

    return { comment, email, phone };
};

const parseId = userId => {
    return new Promise((resolve, reject) => {
        agent.get('https://moikrug.ru/' + userId)
            .withCredentials()
            .set('User-Agent', 'Mozilla/5.0')
            .then(res => {
                fs.writeFile('LAST_HTML_MK.txt', res.text, err => {
                    if (err) {
                        console.error(err);
                    }
                    console.log('Html stored to', 'LAST_HTML_MK.txt');
                });

                const { name, surname } = getFio(res.text, userId);
                const { email, phone, comment } = getContacts(res.text);
                const age = getAge(res.text);
                const city = getCity(res.text);
                const result = {
                    name,
                    surname,
                    age,
                    city,
                    phone,
                    comment,
                    email
                };

                resolve(result);
            })
            .catch(e => reject(e));
    });
};

module.exports = parseId;
