const fs = require('fs');
const request = require('superagent');

let authorized = false;

const agent = request.agent();

agent.post('https://www.facebook.com/login.php')
    .withCredentials()
    .query({
        login_attempt: 1,
        lwv: 110
    })
    .set('upgrade-insecure-requests', '1')
    .set('User-Agent', 'Mozilla/5.0')
    .set('Cookie', 'reg_fb_gate=https%3A%2F%2Fwww.facebook.com%2Flogin%2F; reg_fb_ref=https%3A%2F%2Fwww.facebook.com%2Flogin%2F; _js_reg_fb_ref=https%3A%2F%2Fwww.facebook.com%2Flogin%2F; wd=1920x480')
    .set('content-type', 'application/x-www-form-urlencoded')
    .send({ email: 'razka90@mail.ru', pass: 'logan123' }).then(res => {
        fs.writeFile('AUTH_FB.txt', res.text, err => {
            if (err) {
                console.error(err);
            }
            console.log('Html stored to', 'AUTH_FB.txt');
        });
        if (res.text.indexOf('Alex Ketov') === -1) {
            console.error('Проблемы с авторизацией FB!!!');
        }

        authorized = true;
        console.log(res.statusCode);
    });

const getEmail = text => {
    const emailIdx = text.indexOf('mailto');
    const email = emailIdx !== -1 ?
        text.substring(emailIdx + 7, text.indexOf('>', emailIdx) - 1).replace('%40', '@') :
        '';

    return email;
};

const getFio = (text, userId) => {
    const fioSearchString = `href="https://www.facebook.com/${userId}">`;
    const fioIdx = text.indexOf(fioSearchString);
    const fio = fioIdx !== -1 ?
        text.substring(fioIdx + fioSearchString.length, text.indexOf('</a>', fioIdx)) :
        'None';
    let name, surname;

    if (fio.indexOf(' ') !== -1) {
        [name, surname] = fio.split(' ');
    } else {
        name = fio;
    }

    return {
        name, surname
    };
};

const getBirthDay = text => {
    const searchStr = 'Дата рождения';
    const birthDayIdx = text.indexOf(searchStr);

    if (birthDayIdx !== -1) {
        const offset = 18;
        const startIdx = birthDayIdx + searchStr.length + offset;
        const endIdx = text.indexOf('</div></span>', startIdx);

        return text.substr(startIdx, endIdx - startIdx);
    }

    return '';
};

const getCity = text => {
    const searchStr = 'Живет в';
    const idx = text.indexOf(searchStr);

    if (idx !== -1) {
        const startIdx = text.indexOf('>', idx) + 1;
        const endIdx = text.indexOf('</a>', idx);

        return text.substr(startIdx, endIdx - startIdx);
    }

    return '';
};

const getPhone = text => {
    const searchStr = 'Телефоны';
    const idx = text.indexOf(searchStr);

    if (idx !== -1) {
        const startSearchIdx = text.indexOf('<span', idx);
        const startIdx = text.indexOf('>', startSearchIdx) + 1;
        const endIdx = text.indexOf('</span>', startIdx);

        return text.substr(startIdx, endIdx - startIdx);
    }

    return '';
};

const parseId = userId => {
    return new Promise((resolve, reject) => {
        if (!authorized) {
            // TODO: re-auth with another credentials
            return reject('Пользователь не авторизован');
        }

        return agent.get(`https://www.facebook.com/${userId}/about`)
            .withCredentials()
            .set('User-Agent', 'Mozilla/5.0')
            .query({ section: 'overview' })
            .then(res => {
                fs.writeFile('LAST_HTML.txt', res.text, err => {
                    if (err) {
                        console.error(err);
                    }
                    console.log('Html stored to', 'LAST_HTML.txt');
                });

                const { name, surname } = getFio(res.text, userId);
                const email = getEmail(res.text);
                const birthDay = getBirthDay(res.text);
                const city = getCity(res.text);
                const phone = getPhone(res.text);
                const result = {
                    email,
                    name,
                    surname,
                    birthDay,
                    city,
                    comment: `Facebook: https://www.facebook.com/${userId} (parsed)`,
                    phone
                };

                resolve(result);
            })
            .catch(e => reject(e));
    });
};

module.exports = parseId;
